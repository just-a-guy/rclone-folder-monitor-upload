import collections
import os, hashlib, subprocess, time, datetime
import logging
import traceback
from shlex import quote
import inotify.adapters
from django_rq import job


def du(path):
    """disk usage in human readable format (e.g. '2,1GB')"""
    # logging.info(subprocess.check_output(['du', '-k', path]).split()[0].decode('utf-8'))
    return subprocess.check_output(['du', '-sk', path]).split()[0].decode('utf-8')


class MonitorFolder:

    def __init__(self, folder_to_monitor, logfolder):

        self.log_folder = logfolder
        self.folder_to_monitor = folder_to_monitor
        self.monitoring = {}
        self.wait_time = 2 * 60
        now = datetime.datetime.now()
        time_folder_name = now.strftime("%Y-%m-%d")

        try:
            if not os.path.exists("{}/{}".format(self.log_folder, time_folder_name)):
                os.mkdir("{}/{}".format(self.log_folder, time_folder_name))
        except:
            logging.info("could not create log dir, dumping log in {}/{}".format(self.log_folder, time_folder_name))

        logging.info("Init is completed .....")

    def start_monitor(self):
        i = inotify.adapters.InotifyTree(self.folder_to_monitor.folder_name)

        for event in i.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event

            # logging.info(" {} | {}".format(path, type_names))

            if 'IN_CREATE' in type_names or 'IN_MOVED_TO' in type_names:
                logging.info(filename + path)

                if '_UNPACK_' not in (path + filename):
                    the_path = os.path.join(path, filename)
                    self.long_running_func(the_path)
                else:
                    logging.info("Not gonna monitor this folder : {} ".format(path + filename))

    def trigger_upload(self, folder):

        fullpath = folder
        now = datetime.datetime.now()
        timefoldername = now.strftime("%Y-%m-%d")
        logfolder_to_be_operated = self.log_folder

        try:
            if not os.path.exists("{}/{}".format(self.log_folder, timefoldername)):
                os.mkdir("{}/{}".format(self.log_folder, timefoldername))
        except:
            logging.info("could not create log dir, dumping log in {}/{}".format(self.log_folder, timefoldername))

        # fixing the remote folder's trailing slash since we need one
        remote_sub_folder = folder.replace(self.folder_to_monitor.folder_name, '').strip('/')
        format1 = quote(fullpath)

        if len(self.folder_to_monitor.remote_folder_root) >= 1:
            format2 = quote(
                "{}:{}/{}".format(self.folder_to_monitor.rclone_remote_setup,
                                  self.folder_to_monitor.remote_folder_root,
                                  remote_sub_folder))
        else:
            format2 = quote("{}:{}".format(self.folder_to_monitor.rclone_remote_setup,
                                           remote_sub_folder))

        logfilename = "{} - {}".format(folder.split('/')[-2], folder.split('/')[-1])
        format3 = quote(logfilename)

        command = "nohup rclone copy {} {} -v --stats=2s > {}/{}/{}.log 2>&1 &". \
            format(format1, format2, logfolder_to_be_operated, timefoldername, format3)

        # logging.info(command)

        # os.system(command)
        # self.processing.append(strmd5)
        # del self.sizeMonitor[strmd5]
        logging.info("now executing : " + command)

    @job('high')
    def long_running_func(self, full_path):

        try:
            test = self.du(full_path)
        except:
            logging.info("Looks like folder {} was not found".format(full_path))
            return "nope, folder not found."

        if full_path in self.monitoring:
            logging.info(" The path {} is already being monitored".format(full_path))
            self.monitoring[full_path]['__time__'] = time.time()
            self.monitoring[full_path]['__size__'] = self.du(full_path)
            return "Already being monitored"
        else:
            self.monitoring[full_path] = {}
            self.monitoring[full_path]['__time__'] = time.time()
            self.monitoring[full_path]['__size__'] = self.du(full_path)

        logging.info(" To upload {} need to wait {} seconds".format(full_path,
                                                                    self.wait_time -
                                                                    (time.time() -
                                                                     self.monitoring[full_path]['__time__'])))

        # just making it easier to use in following places.
        created_time = self.monitoring[full_path]['__time__']

        while (time.time() - created_time) < self.wait_time:
            time.sleep(30)
            logging.info(" To upload {} need to wait {} seconds".format(full_path,
                                                                        self.wait_time - (time.time() - created_time)))

            if self.monitoring[full_path]['__size__'] != self.du(full_path):
                self.monitoring[full_path]['__time__'] = time.time()
                logging.info(" The folder size for {} just changed from {}KB to {}KB. Therefore time is now updated.".
                             format(full_path, self.monitoring[full_path]['__size__'], self.du(full_path)))
                self.monitoring[full_path]['__size__'] = self.du(full_path)

        # when it comes here, the wait is over.
        del self.monitoring[full_path]
        # self.trigger_upload(full_path)

    def du(self, path):
        """disk usage in human readable format (e.g. '2,1GB')"""
        # logging.info(subprocess.check_output(['du', '-k', path]).split()[0].decode('utf-8'))
        return subprocess.check_output(['du', '-sk', path]).split()[0].decode('utf-8')

    def stop(self):
        pass
