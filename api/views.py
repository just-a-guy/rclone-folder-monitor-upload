import logging
import logging.handlers
import os
from configparser import ConfigParser

from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .Helpers.StoppableThread import StoppableThread
from .Helpers.MonitorFolder import MonitorFolder
from .models import FolderType, Folders

path_to_upload = []


# def init_logging():
config = ConfigParser()
filename = os.path.join(os.path.dirname(__file__), 'config/settings.ini')
config.read(filename)
logfolder = config.get('loginfo', 'logfolder')

handler = logging.handlers.WatchedFileHandler(os.environ.get("LOGFILE", "{}/awesome.log".format(logfolder)))

formatter = logging.Formatter(logging.BASIC_FORMAT)
handler.setFormatter(formatter)
root = logging.getLogger()
root.setLevel("INFO")
root.addHandler(handler)

monitored_folders = []

def fail(data):
    return Response({"status": False, 'data': data})


def passed(data):
    return Response({"status": True, 'data': data})


@api_view(['POST'])
@parser_classes((JSONParser, JSONRenderer))
def rclone_upload(request):
    Data = request.data
    required_params = ['local_path', 'type', 'rclone_remote', 'remote_root']
    if request.method == 'POST' and all(x in Data.keys() for x in required_params):

        if Data['type'] not in [tag[0] for tag in FolderType.choices()]:
            return fail({'message': 'Type not in accepted types.'})

        if not os.path.exists(Data['local_path']):
            return fail({'message': 'Path \'{}\' does not exist'.format(Data['local_path'])})

        path_to_upload.append((Data['local_path'], Data['type']))

        nwentry = Folders.objects.create(folder_name=Data['local_path'],
                                         remote_folder_root=Data['remote_root'],
                                         rclone_remote_setup=Data['rclone_remote'],
                                         folder_type=Data['type'])

        nwentry.save()
        return passed({"message": "accepted"})


@api_view(['GET'])
@parser_classes((JSONParser, JSONRenderer))
def monitor_folder(request):

    all_entries = Folders.objects.all()

    for each_entry in all_entries:

        if each_entry.folder_name not in monitored_folders:
            w = MonitorFolder(each_entry, logfolder)
            x = StoppableThread(function=w)
            x.start()
            monitored_folders.append(each_entry.folder_name)

    return passed({"message": monitored_folders})
