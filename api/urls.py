from django.urls import path

from . import views

urlpatterns = [
    path('rclone-upload', views.rclone_upload),
    path('start-monitor', views.monitor_folder)
]


