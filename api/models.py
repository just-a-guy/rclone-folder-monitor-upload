from django.db import models
from enumfields import EnumField
from enumfields import Enum


# Create your models here.

class FolderType(Enum):
    TV = 'TV'
    MOVIES = 'MOVIES'


class Folders(models.Model):
    id = models.AutoField(primary_key=True)
    folder_name = models.TextField()
    folder_type = EnumField(FolderType, max_length=10)
    rclone_remote_setup = models.TextField()
    remote_folder_root = models.TextField()




